package config

type Provider struct {
	ServiceAccountFile string

	Project           string
	Zone              string
	MachineType       string
	MinCPUPlatform    string
	Image             string
	DiskSize          int64
	DiskType          string
	Network           string
	Subnetwork        string
	UseInternalIPOnly bool

	Tags []string

	Username string
}
