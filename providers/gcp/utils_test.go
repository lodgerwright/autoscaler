package gcp

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGlobalResourceURI(t *testing.T) {
	expectedURI := "https://www.googleapis.com/compute/v1/projects/test-project/global/networks/default"
	assert.Equal(t, expectedURI, globalResourceURI("test-project", resourceNetwork, "default"))
}

func TestRegionResourceURI(t *testing.T) {
	expectedURI := "https://www.googleapis.com/compute/v1/projects/test-project/regions/us-east1/networks/default"
	assert.Equal(t, expectedURI, regionResourceURI("test-project", "us-east1", resourceNetwork, "default"))
}

func TestZoneResourceURI(t *testing.T) {
	expectedURI := "https://www.googleapis.com/compute/v1/projects/test-project/zones/us-east1-c/networks/default"
	assert.Equal(t, expectedURI, zoneResourceURI("test-project", "us-east1-c", resourceNetwork, "default"))
}

func TestZoneToRegion(t *testing.T) {
	tests := []struct {
		zone           string
		expectedRegion string
		expectedErr    bool
	}{
		{
			zone:           "us-east1-c",
			expectedRegion: "us-east1",
			expectedErr:    false,
		},
		{
			zone:           "europe-west4-c",
			expectedRegion: "europe-west4",
			expectedErr:    false,
		},
		{
			zone:           "asia-northeast2-b",
			expectedRegion: "asia-northeast2",
			expectedErr:    false,
		},
		{
			zone:           "us-central1-f",
			expectedRegion: "us-central1",
			expectedErr:    false,
		},
		{
			zone:        "someInvalidRegion",
			expectedErr: true,
		},
		{
			zone:        "asia1-northeast2-b",
			expectedErr: true,
		},
		{
			zone:        "us-central1-ff",
			expectedErr: true,
		},
		{
			zone:        "us-central*-f",
			expectedErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.zone, func(t *testing.T) {
			region, err := zoneToRegion(tt.zone)
			if tt.expectedErr {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expectedRegion, region)
		})
	}

}
