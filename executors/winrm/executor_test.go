package winrm

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"strings"
	"testing"
	"time"

	"github.com/masterzen/winrm"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging/test"
	mocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/executors/winrm"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

var (
	testConfig = config.Global{}
	testVM     = vm.Instance{}
)

func mockNewClient(t *testing.T, mockedError error) (*mocks.Client, func()) {
	c := new(mocks.Client)

	oldNewClient := newClient
	cleanup := func() {
		newClient = oldNewClient
		c.AssertExpectations(t)
	}

	newClient = func(endpoint *winrm.Endpoint, user string, password string) (Client, error) {
		return c, mockedError
	}

	return c, cleanup
}

func mockAvailabilityCheckTimeout() func() {
	oldAvailabilityCheckTimeout := availabilityCheckTimeout
	cleanup := func() {
		availabilityCheckTimeout = oldAvailabilityCheckTimeout
	}

	availabilityCheckTimeout = 1 * time.Millisecond

	return cleanup
}

func TestExecutor_Execute(t *testing.T) {
	tests := map[string]struct {
		newClientError       error
		enableListener       bool
		serviceCheckExitCode int
		scriptExitCode       int
		scriptError          error
		expectedError        error
	}{
		"new client error": {
			newClientError: errors.New("test error"),
			expectedError:  errors.New(`couldn't create a WinRM client for Virtual Machine "": test error`),
		},
		"port availability check failure": {
			enableListener:       false,
			serviceCheckExitCode: 0,
			expectedError:        errWinRMServicePortUnavailable,
		},
		"service availability check failure": {
			enableListener:       true,
			serviceCheckExitCode: 1,
			expectedError:        errWinRMServiceUnavailable,
		},
		"script execution error": {
			enableListener:       true,
			serviceCheckExitCode: 0,
			scriptError:          errors.New("test error"),
			scriptExitCode:       0,
			expectedError:        errors.New(`build failure: couldn't run WinRM command on the Virtual Machine "": test error`),
		},
		"script execution invalid exit code": {
			enableListener:       true,
			serviceCheckExitCode: 0,
			scriptError:          nil,
			scriptExitCode:       1,
			expectedError:        errors.New(`build failure: script execution on the Virtual Machine "" exited with 1 error code`),
		},
		"script execution valid": {
			enableListener:       true,
			serviceCheckExitCode: 0,
			scriptError:          nil,
			scriptExitCode:       0,
			expectedError:        nil,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			clientMock, cleanup := mockNewClient(t, testCase.newClientError)
			defer cleanup()

			actCleanup := mockAvailabilityCheckTimeout()
			defer actCleanup()

			if testCase.enableListener {
				listener, err := net.Listen("tcp", fmt.Sprintf("127.0.0.1:%d", port))
				require.NoError(t, err)

				defer listener.Close()

				// service availability check call
				serviceAvailabilityCheckMatcher := mock.MatchedBy(func(r *bytes.Buffer) bool {
					script, _ := ioutil.ReadAll(r)
					_, _ = r.Write(script)

					return !strings.Contains(string(script), "test-script")
				})

				clientMock.
					On("RunWithInput", powershellCmd, mock.Anything, mock.Anything, serviceAvailabilityCheckMatcher).
					Return(testCase.serviceCheckExitCode, nil)

				if testCase.serviceCheckExitCode == 0 {
					// script execution call
					scriptMatcher := mock.MatchedBy(func(r *bytes.Buffer) bool {
						script, _ := ioutil.ReadAll(r)
						_, _ = r.Write(script)

						return strings.Contains(string(script), "test-script")
					})

					clientMock.
						On("RunWithInput", powershellCmd, mock.Anything, mock.Anything, scriptMatcher).
						Return(testCase.scriptExitCode, testCase.scriptError)
				}
			}

			logger := test.NewNullLogger()

			e, err := New(testConfig, logger)
			assert.NoError(t, err)
			assert.IsType(t, &Executor{}, e)

			vmInst := vm.Instance{
				IPAddress: "127.0.0.1",
			}

			err = e.Execute(context.Background(), vmInst, []byte("test-script"))
			if testCase.expectedError == nil {
				assert.NoError(t, err)
				return
			}

			assert.Equal(t, testCase.expectedError.Error(), fmt.Sprintf("%v", err))
		})
	}
}

func TestExecutor_ProvisionScript(t *testing.T) {
	e, err := New(testConfig, logging.New())
	assert.NoError(t, err)
	assert.IsType(t, &Executor{}, e)

	script := e.ProvisionScript()
	assert.Equal(t, initWinRMScript, script)
}
