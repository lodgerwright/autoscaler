package executors

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/factories"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging/test"
	mocks "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/executors"
)

var (
	testFactoryType  = "executor mock"
	testExecutorName = "test-executor"
	testExecutor     = new(mocks.Executor)
	errTest          = errors.New("test-error")

	testFactory = func(cfg config.Global, logger logging.Logger) (Executor, error) {
		return testExecutor, errTest
	}
)

func mockFactoriesRegistry() func() {
	oldFactoriesRegistry := factoriesRegistry
	cleanup := func() {
		factoriesRegistry = oldFactoriesRegistry
	}

	factoriesRegistry = factories.NewRegistry(testFactoryType)

	return cleanup
}

func TestMustRegister(t *testing.T) {
	defer mockFactoriesRegistry()()

	assert.NotPanics(t, func() {
		MustRegister(testExecutorName, testFactory)
	})

	assert.True(t, factoriesRegistry.IsRegistered(testExecutorName))
}

func TestMustRegister_DoubledRegistration(t *testing.T) {
	defer mockFactoriesRegistry()()

	MustRegister(testExecutorName, testFactory)

	assert.Panics(t, func() {
		MustRegister(testExecutorName, testFactory)
	})

	assert.True(t, factoriesRegistry.IsRegistered(testExecutorName))
}

func TestFactorize(t *testing.T) {
	defer mockFactoriesRegistry()()

	require.NotPanics(t, func() {
		MustRegister(testExecutorName, testFactory)
	})

	cfg := config.Global{
		Executor: testExecutorName,
	}
	logger := test.NewNullLogger()

	executor, err := Factorize(cfg, logger)
	assert.Equal(t, testExecutor, executor)
	assert.Equal(t, errTest, err)
}

func TestFactorize_UnknownExecutor(t *testing.T) {
	defer mockFactoriesRegistry()()

	cfg := config.Global{
		Executor: testExecutorName,
	}
	logger := test.NewNullLogger()

	executor, err := Factorize(cfg, logger)
	assert.Nil(t, executor)
	assert.Equal(t, factories.NewErrNotRegistered(testFactoryType, testExecutorName), err)
}
