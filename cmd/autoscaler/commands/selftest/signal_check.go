package selftest

import (
	stdContext "context"
	"sync"
	"time"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
)

const (
	defaultSignalCheckTimeout = 5
)

func NewSignalCheckCommand() cli.Command {
	return cli.Command{
		Config: cli.Config{
			Name:    "signal-check",
			Aliases: []string{"sc"},
			Usage:   "Allows to test termination signal handling",
		},
		Handler: &SignalCheckCommand{
			Timeout: defaultSignalCheckTimeout,
		},
	}
}

type SignalCheckCommand struct {
	Timeout int `long:"timeout" description:"Maximum timeout of command execution (in seconds)"`
}

func (c *SignalCheckCommand) Execute(ctx *cli.Context) error {
	wg := new(sync.WaitGroup)
	wg.Add(1)

	go c.waitForSignalOrTimeout(ctx.Ctx, ctx.Logger(), wg)

	wg.Wait()

	for i := 0; i < 5; i++ {
		ctx.
			Logger().
			Info("Proceeding termination loop")
		time.Sleep(1 * time.Second)
	}

	return nil
}

func (c *SignalCheckCommand) waitForSignalOrTimeout(ctx stdContext.Context, logger logging.Logger, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := 0; i < c.Timeout; i++ {
		select {
		case <-ctx.Done():
			logger.Info("Received signal; exiting")

			return
		case <-time.After(1 * time.Second):
			logger.Info("Waiting for signal or timeout...")
		}
	}

	logger.Info("Timeout reached; exiting")
}
